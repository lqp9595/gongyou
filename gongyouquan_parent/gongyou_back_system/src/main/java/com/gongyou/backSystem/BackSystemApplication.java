package com.gongyou.backSystem;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @Author LiQuanPing
 * @Date 2021-04-20 10:28
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
@EnableDiscoveryClient
@EnableFeignClients
@SpringBootApplication
@EnableEurekaClient
@MapperScan("com.gongyou.backSystem.mapper")
public class BackSystemApplication {
    public static void main(String[] args) {
        SpringApplication.run(BackSystemApplication.class,args);
    }
}
