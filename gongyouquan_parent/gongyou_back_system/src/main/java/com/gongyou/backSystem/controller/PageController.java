package com.gongyou.backSystem.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Author LiQuanPing
 * @Date 2021-04-20 10:35
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 * 后台管理
 */
@Controller("/")
public class PageController {

    @RequestMapping("/{pageName}")
    public String returnPage(@PathVariable String pageName){
        return pageName;
    }

}
