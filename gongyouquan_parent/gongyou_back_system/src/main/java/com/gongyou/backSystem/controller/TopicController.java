package com.gongyou.backSystem.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gongyou.backSystem.feign.CircleFeign;
import com.gongyou.backSystem.service.TopicService;
import com.gongyou.common.entity.QueryPage;
import com.gongyou.common.pojo.Topic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author LiQuanPing
 * @Date 2021-04-21 12:40
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
@RestController
@RequestMapping("/topic")
public class TopicController {

    @Autowired
    private TopicService topicService;

    @Autowired
    private CircleFeign circleFeign;

    //分页查询topic表中的所有信息
    @RequestMapping("/findPage")
    public Page findPage(@RequestBody QueryPage queryPage) {
        return topicService.findPage(queryPage);
    }


    //根据id查询topic所有信息
    @GetMapping("/getTopicDeatilById/{id}")
    public List getTopicDeatilById(@PathVariable("id") String id) {
        return circleFeign.getCommentById(id);

    }


}
