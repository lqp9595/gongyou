package com.gongyou.backSystem.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gongyou.backSystem.service.UserService;
import com.gongyou.common.entity.QueryPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author LiQuanPing
 * @Date 2021-04-20 12:35
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("/findPage")
    public Page findPage(@RequestBody QueryPage queryPage){
        return userService.findPage(queryPage);
    }

}
