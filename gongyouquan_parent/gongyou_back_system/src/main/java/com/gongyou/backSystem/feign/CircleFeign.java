package com.gongyou.backSystem.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @Author LiQuanPing
 * @Date 2021-04-22 10:13
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
@FeignClient(name = "forum")
public interface CircleFeign {

    @RequestMapping("/circle/getCommentById/{id}")
    public List getCommentById(@PathVariable("id") String id);


}
