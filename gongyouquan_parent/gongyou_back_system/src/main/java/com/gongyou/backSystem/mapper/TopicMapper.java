package com.gongyou.backSystem.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gongyou.common.pojo.Topic;

/**
 * @Author LiQuanPing
 * @Date 2021-04-21 12:40
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
public interface TopicMapper extends BaseMapper<Topic> {
}
