package com.gongyou.backSystem.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gongyou.common.pojo.User;

/**
 * @Author LiQuanPing
 * @Date 2021-04-20 12:36
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
public interface UserMapper extends BaseMapper<User> {
}
