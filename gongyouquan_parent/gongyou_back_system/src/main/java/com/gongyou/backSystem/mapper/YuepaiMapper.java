package com.gongyou.backSystem.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gongyou.common.pojo.Yuepai;

/**
 * @Author LiQuanPing
 * @Date 2021-04-21 12:42
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
public interface YuepaiMapper extends BaseMapper<Yuepai> {
}
