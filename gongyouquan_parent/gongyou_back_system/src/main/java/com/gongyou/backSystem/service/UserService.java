package com.gongyou.backSystem.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gongyou.common.entity.QueryPage;

/**
 * @Author LiQuanPing
 * @Date 2021-04-20 12:36
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
public interface UserService {

    Page findPage(QueryPage queryPage);

}
