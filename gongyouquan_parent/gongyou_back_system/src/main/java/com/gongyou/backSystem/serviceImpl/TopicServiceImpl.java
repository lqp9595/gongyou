package com.gongyou.backSystem.serviceImpl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gongyou.backSystem.mapper.TopicMapper;
import com.gongyou.backSystem.service.TopicService;
import com.gongyou.common.entity.QueryPage;
import com.gongyou.common.pojo.Topic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author LiQuanPing
 * @Date 2021-04-21 12:41
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
@Service
public class TopicServiceImpl implements TopicService {

    @Autowired
    private TopicMapper topicMapper;

    @Override
    public Page findPage(QueryPage queryPage) {
        String queryString = queryPage.getQueryString().trim();
        Page page1 = new Page(queryPage.getCurrentPage(),queryPage.getPageSize());
        Page page = topicMapper.selectPage(page1, new QueryWrapper<Topic>().eq(!queryString.equals(""),"u_name",queryString));
        List<Topic> records = page.getRecords();
        for (Topic record : records) {
            record.setPocUrl(JSON.parseObject(record.getUrl(),List.class));
        }
        return page.setRecords(records);
    }
}
