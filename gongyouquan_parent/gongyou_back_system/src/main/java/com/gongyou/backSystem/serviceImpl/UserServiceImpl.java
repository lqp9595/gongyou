package com.gongyou.backSystem.serviceImpl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gongyou.backSystem.mapper.UserMapper;
import com.gongyou.backSystem.service.UserService;
import com.gongyou.common.entity.QueryPage;
import com.gongyou.common.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author LiQuanPing
 * @Date 2021-04-20 12:37
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public Page findPage(QueryPage queryPage) {
        String queryString = queryPage.getQueryString().trim();
        Page page = new Page(queryPage.getCurrentPage(), queryPage.getPageSize());
        return userMapper.selectPage(page, new QueryWrapper<User>()
                .eq("is_del", 0)
                .eq(!queryString.equals(""), "phone", queryString)
                .orderByDesc("register_time"));

    }
}
