package com.gongyou.backSystem.serviceImpl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gongyou.backSystem.mapper.YuepaiMapper;
import com.gongyou.backSystem.service.YuepaiService;
import com.gongyou.common.entity.QueryPage;
import com.gongyou.common.pojo.Yuepai;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author LiQuanPing
 * @Date 2021-04-21 12:42
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
@Service
public class YuepaiServiceImpl implements YuepaiService {
    @Autowired
    private YuepaiMapper yuepaiMapper;

    @Override
    public Page findPage(QueryPage queryPage) {
        String queryString = queryPage.getQueryString().trim();
        Page page1 = new Page(queryPage.getCurrentPage(),queryPage.getPageSize());
        Page page = yuepaiMapper.selectPage(page1, new QueryWrapper<Yuepai>()
                    .eq(!queryString.equals(""),"p_name",queryString));
        List<Yuepai> records = page.getRecords();
        for (Yuepai record : records) {
            record.setUrlsList(JSON.parseObject(record.getUrls(),List.class));
            record.setPTagsList(JSON.parseObject(record.getPTags(),List.class));
        }
        return page.setRecords(records);
    }
}
