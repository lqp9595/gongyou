var base = "/backsystem";

function getBackUrl() {
    return base;
}



function check() {
    // 请求拦截
    axios.interceptors.request.use(config => {
        let token = window.localStorage.getItem("token");
       // axios.defaults.headers.common["token"] = token;
        config.headers.token = token;
        return config
    },error => {
        return Promise.reject(error)
    })
    axios.interceptors.response.use(response => {
        // 预处理相应的数据
        return response
    }, error => {
        if (error.response.status === 401){
            // alert(error.response.status);
            alert("登录过期,请重新登录!!!");
            // 错误返回 状态码验证
            localStorage.removeItem("token");
            top.location.href ="login.html";
        }else if (error.response.status === 420) {
            alert("你没有此操作对应的权限,请联系管理员!!!");
        }
        return Promise.reject(error)
    });
   // window.location.href   //是本页面跳转
   // parent.location.href   //是上一层页面跳转
   // top.location.href      //是最外层的页面跳转
}