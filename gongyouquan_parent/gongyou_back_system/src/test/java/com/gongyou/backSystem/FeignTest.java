package com.gongyou.backSystem;

import com.gongyou.backSystem.feign.CircleFeign;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @Author LiQuanPing
 * @Date 2021-04-22 10:29
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BackSystemApplication.class)
public class FeignTest {

    @Autowired
    private CircleFeign circleFeign;

    @Test
    public void test1(){

        List commentById = circleFeign.getCommentById("34534534");
        System.out.println(commentById);

    }


}
