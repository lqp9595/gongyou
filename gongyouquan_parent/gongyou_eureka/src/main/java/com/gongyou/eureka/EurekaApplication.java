package com.gongyou.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @Author LiQuanPing
 * @Date 2021-03-19 11:43
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 * 注册中心
 */
@SpringBootApplication
@EnableEurekaServer
public class EurekaApplication {
    public static void main(String[] args) {
        SpringApplication.run(EurekaApplication.class,args);
    }
}
