package com.gongyou.forum;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @Author LiQuanPing
 * @Date 2021-03-19 12:27
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 * 圈子业务模块,启动类
 */
@SpringBootApplication
@EnableEurekaClient
@MapperScan("com.gongyou.forum.mapper")
@EnableTransactionManagement
public class ForumApplication {
    public static void main(String[] args) {
        SpringApplication.run(ForumApplication.class,args);
    }
}
