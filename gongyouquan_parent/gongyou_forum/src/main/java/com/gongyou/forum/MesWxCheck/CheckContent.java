package com.gongyou.forum.MesWxCheck;

import com.gongyou.common.utils.HttpUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;
import com.alibaba.fastjson.JSONObject;

/**
 * @Author LiQuanPing
 * @Date 2021-04-24 11:26
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
@RestController
@RequestMapping("/mesCheck")
public class CheckContent {
    private String url = "https://api.weixin.qq.com/wxa/msg_sec_check?access_token=";


    @GetMapping("/getData")
    public Object getData(String content) throws UnsupportedEncodingException{
        JSONObject json = new JSONObject();
        json.put("content", content);
        String result = HttpUtil.doPost(url, json.toJSONString());
        String errcode = result.toString().split(",")[0].split(":")[1];
        //过期access_token（42001）；没有access_token（41001）；无效access_token（40001）
        if(Integer.parseInt(errcode) == 42001 || Integer.parseInt(errcode) == 41001 || Integer.parseInt(errcode) == 40001) {
            //重新获取access_token(接口调用凭证)
            System.out.println("重新获取access_token(接口调用凭证)");
         //   Object access = HttpUtil.doGet("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=开发设置中的AppID&secret=开发设置中的AppSecret");//
            Object access = HttpUtil.doGet("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wxba1e4d5f7f7edd1d&secret=a30783d6a78b4fb64a3a1a8cf7753f4d");//
            System.out.println("access==="+access);
            String access_token = access.toString().split(",")[0].split(":")[1];
            access_token = access_token.substring(1,access_token.length()-1);
            url = "https://api.weixin.qq.com/wxa/msg_sec_check?access_token="+access_token;
            result = HttpUtil.doPost(url,json.toJSONString());
        }
        return result;
    }

}










