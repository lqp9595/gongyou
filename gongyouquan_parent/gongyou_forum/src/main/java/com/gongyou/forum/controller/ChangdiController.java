package com.gongyou.forum.controller;

import com.gongyou.common.entity.Result;
import com.gongyou.common.pojo.Changdi;
import com.gongyou.forum.service.ChangdiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author LiQuanPing
 * @Date 2021-04-12 19:42
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
@RestController
@RequestMapping("/changdi")
public class ChangdiController {

    @Autowired
    private ChangdiService changdiService;

    //获取所有场地列表
    @RequestMapping("/getChangdiAll")
    public List getChangdiAll(){
        return changdiService.getChangdiAll();
    }

    //获取id获取场地详情
    @RequestMapping("/getChangdiDetail")
    public Changdi getChangdiDetail(String id){
        return changdiService.getChangdiDetail(id);
    }

    //增加场地浏览量
    @RequestMapping("/addChangdiView")
    public void addChangdiView(String id){
         changdiService.addChangdiView(id);
    }

    //发布场地相册 保存信息
    @RequestMapping("/pulishChangdi")
    public Result pulishChangdi(@RequestBody Changdi changdi){
        try {
            changdiService.pulishChangdi(changdi);
            return new Result(true, "ok",1);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "error",0);
        }
    }

}
