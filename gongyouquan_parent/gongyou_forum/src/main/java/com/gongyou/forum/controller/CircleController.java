package com.gongyou.forum.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gongyou.common.entity.Result;
import com.gongyou.common.pojo.Topic;
import com.gongyou.common.utils.QiniuUtils;
import com.gongyou.forum.service.CircleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.*;

/**
 * @Author LiQuanPing
 * @Date 2021-03-19 16:38
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
@Slf4j
@RequestMapping("/circle")
@RestController
public class CircleController {

    @Autowired
    private CircleService circleService;

    //读取圈子所有列表
    @RequestMapping("/getCircle")
    public Page getCircle( String userId, String pageNum) {
       // log.info(userId+"======"+pageNum);
        return circleService.getCircle(userId,pageNum);
    }
    //读取我关注的圈子所有列表
    @RequestMapping("/getFollow")
    public Page getFollow(String userId, String pageNum) {
       return circleService.getFollow(userId,pageNum);
    }

    //根据图片id获取图片信息,用于展示评论和回复用
    @RequestMapping("/getCommentById/{id}")
    public List getCommentById(@PathVariable("id") String id) {
        return circleService.getCommentById(id);
    }

    //发布圈子动态,保存发布的信息
    @RequestMapping("/publishCircle")
    public Result publishCircle(@RequestBody Topic topic) {
        // Integer count = circleService.publishCircle();
        try {
            System.out.println(topic);
            circleService.publishCircle(topic);
            return new Result(true, "ok");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "error");
        }
    }
    //根据userId读取我的圈子所有列表
    @RequestMapping("/getCircleByUserId")
    public Result getCircleByUserId(String userId) {
        List list =  circleService.getCircleByUserId(userId);
        return new Result(true,"ok",list);
    }
    //根据id删除topic圈子信息
    @RequestMapping("/delTopicById")
    public void delTopicById(String tid) {
     circleService.delTopicById(tid);
    }

    //读取我收藏的topic列表
    @RequestMapping("/getTopicByShoucangId")
    public List getTopicByShoucangId( String userId) {
        return circleService.getTopicByShoucangId(userId);
    }
}
