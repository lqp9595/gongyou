package com.gongyou.forum.controller;

import com.gongyou.common.entity.Result;
import com.gongyou.common.pojo.Dianzan;
import com.gongyou.forum.service.DianzanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author LiQuanPing
 * @Date 2021-03-31 20:47
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
@RestController
@RequestMapping("/dianzan")
public class DianzanController {

    @Autowired
    private DianzanService dianzanService;

    @PostMapping("/addDianzan")
    public Result addDianzan(@RequestBody Dianzan dianzan){
        try {
            dianzanService.addDianzan(dianzan);
            return new Result(true, "ok", 1);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "ok", 0);
        }
    }

    @PostMapping("/cancelDianzan")
    public Result cancelDianzan(@RequestBody Dianzan dianzan){
        try {
            dianzanService.cancelDianzan(dianzan);
            return new Result(true, "ok", 1);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "ok", 0);
        }
    }


}
