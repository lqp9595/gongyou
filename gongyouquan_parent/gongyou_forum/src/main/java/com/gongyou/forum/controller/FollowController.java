package com.gongyou.forum.controller;

import com.gongyou.common.entity.Result;
import com.gongyou.forum.service.FollowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @Author LiQuanPing
 * @Date 2021-03-26 14:27
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
@RestController
@RequestMapping("/follow")
public class FollowController {

    @Autowired
    private FollowService followService;

    //判断是否关注  需用自己的id和用户的id
    @RequestMapping("/isFollowByUserId")
    public Result isFollowByUserId(String userId, String myId) {
        boolean flag = followService.isFollowByUserId(userId, myId);
        return new Result(true, "ok", flag);
    }


    //关注约拍用户
    @RequestMapping("/FollowYuePaiById")
    public Result FollowYuePaiById(@RequestBody Map map) {
        try {
            followService.FollowYuePaiById(map);
            return new Result(true,"ok",1);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"error",0);
        }
    }

    //取消关注约拍用户
    @RequestMapping("/cancelFollowYuePaiById")
    public Result cancelFollowYuePaiById(String uid,String fid) {
        try {
            followService.cancelFollowYuePaiById(uid,fid);
            return new Result(true,"ok",1);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"error",0);
        }
    }

    //根据id查找用户的粉丝
    @RequestMapping("/findFansByid")
    public List findFansByid(String userId) {
        return followService.findFansById(userId);
    }

    //根据id查找用户的关注
    @RequestMapping("/findFollowByid")
    public List findFollowByid(String userId) {
        return followService.findFollowByid(userId);
    }
}
