package com.gongyou.forum.controller;

import com.gongyou.forum.service.MesService;
import com.netflix.discovery.converters.Auto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author LiQuanPing
 * @Date 2021-03-30 13:39
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
@RestController
@RequestMapping("/mes")
public class MesController {


    @Autowired
    private MesService mesService;

    //根据id查找消息
    @GetMapping("/findMesById/{id}")
    public List findMesById(@PathVariable("id")String id){
        return mesService.findMesById(id);

    }
}
