package com.gongyou.forum.controller;

import com.gongyou.common.entity.Result;
import com.gongyou.common.pojo.Reply;
import com.gongyou.forum.service.ReplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author LiQuanPing
 * @Date 2021-03-30 10:07
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
@RestController
@RequestMapping("/reply")
public class ReplyController {

    @Autowired
    private ReplyService replyService;

    //根据图片id读取回复信息
    @GetMapping("/getReplyByTopicId/{topicId}")
    public List getReplyByTopicId(@PathVariable("topicId") String topicId) {
        return replyService.getReplyByTopicId(topicId);
    }

    //对当前发图片的用户发表评论
    @PostMapping("/insertReplyInfo/{topicUserId}")
    public Result insertReplyInfo(@RequestBody Reply reply, @PathVariable("topicUserId") String topicUserId) {
        try {
            replyService.insertReplyInfo(reply, topicUserId);
            // System.out.println(reply+"==="+topicUserId);
            return new Result(true, "ok", 1);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "error", 0);
        }
    }

    //删除评论信息
    @GetMapping("/deleteReplyAndMes")
    public Result deleteReplyAndMes(String id,String tid,String time) {
        try {
            replyService.deleteReplyAndMes(id,tid,time);
            return new Result(true, "ok", 1);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "error", 0);
        }
    }
}
