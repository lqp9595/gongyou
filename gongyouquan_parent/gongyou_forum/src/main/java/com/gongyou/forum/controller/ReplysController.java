package com.gongyou.forum.controller;

import com.gongyou.common.entity.Result;
import com.gongyou.common.pojo.Replys;
import com.gongyou.forum.service.ReplysService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author LiQuanPing
 * @Date 2021-03-30 10:57
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
@RestController
@RequestMapping("/replys")
public class ReplysController {

    @Autowired
    private ReplysService replysService;

    //根据图片id读取多级回复信息
    @GetMapping("/getReplyByTopicId/{topicId}")
    public List getReplyByTopicId(@PathVariable("topicId") String topicId){
        return replysService.getReplyByTopicId(topicId);
    }


    //对当前发图片的用户发表多级评论
    @PostMapping("/insertReplysInfo/{topicUserId}")
    public Result insertReplysInfo(@RequestBody Replys replys, @PathVariable("topicUserId") String topicUserId){
        try {
           replysService.insertReplysInfo(replys,topicUserId);
           // System.out.println(replys+"==="+topicUserId);
            return new Result(true,"ok",1);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"error",0);
        }
    }


    //删除多级评论信息
    @GetMapping("/deleteReplysAndMes")
    public Result deleteReplyAndMes(String id,String tid,String time) {
        try {
            replysService.deleteReplysAndMes(id,tid,time);
            return new Result(true, "ok", 1);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "error", 0);
        }
    }
}
