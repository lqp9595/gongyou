package com.gongyou.forum.controller;

import com.gongyou.common.entity.Result;
import com.gongyou.common.pojo.Shoucang;
import com.gongyou.forum.service.ShoucangService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author LiQuanPing
 * @Date 2021-03-30 17:16
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
@RestController
@RequestMapping("/shouCang")
public class ShoucangController {

    @Autowired
    private ShoucangService shoucangService;

    //收藏
    @PostMapping("/addSc")
    public Result addSc(@RequestBody Shoucang shoucang){
        try {
            shoucangService.addSc(shoucang);
            return new Result(true, "ok", 1);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "error", 0);
        }

    }
    //取消收藏
    @PostMapping("/deleteSc")
    public Result deleteSc(@RequestBody Shoucang shoucang){
        try {
            shoucangService.deleteSc(shoucang);
            return new Result(true, "ok", 1);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "error", 0);
        }

    }

}
