package com.gongyou.forum.controller;

import com.gongyou.common.entity.Result;
import com.gongyou.forum.service.ShoucangYService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author LiQuanPing
 * @Date 2021-04-09 15:48
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
@RestController
@RequestMapping("/shoucangY")
public class ShoucangYController {

    @Autowired
    private ShoucangYService shoucangYService;

    //添加约拍收藏
    @RequestMapping("/addShoucangY")
    public Result addShoucangY(String yId, String userId) {
        try {
            shoucangYService.addShoucangY(yId, userId);
            return new Result(true, "ok", 1);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "ok", 0);
        }

    }
    //取消约拍收藏
    @RequestMapping("/cancelShoucangY")
    public Result cancelShoucangY(String yId, String userId) {
        try {
            shoucangYService.cancelShoucangY(yId, userId);
            return new Result(true, "ok", 1);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "ok", 0);
        }

    }

}
