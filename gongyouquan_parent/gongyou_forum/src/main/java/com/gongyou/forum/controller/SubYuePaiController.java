package com.gongyou.forum.controller;

import com.gongyou.common.entity.Result;
import com.gongyou.common.pojo.Suby;
import com.gongyou.forum.service.SubYuePaiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author LiQuanPing
 * @Date 2021-04-08 15:37
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
@RequestMapping("/suby")
@RestController
public class SubYuePaiController {

    @Autowired
    private SubYuePaiService subYuePaiService;


    //发布约拍请求
    @RequestMapping("/submitYuePai")
    public Result submitYuePai(@RequestBody Suby suby) {
        //System.out.println(suby);
        try {
            subYuePaiService.submitYuePai(suby);
            return new Result(true, "ok", 1);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "ok", 0);
        }


    }

    //读取我发布的约拍信息
    @RequestMapping("/getYuePaiMessage")
    public List getYuePaiMessage(String userId) {
        return subYuePaiService.getYuePaiMessage(userId);
    }

    //读取我收到的约拍信息
    @RequestMapping("/getYuePaiMessageByUid")
    public List getYuePaiMessageByUid(String userId) {
        return subYuePaiService.getYuePaiMessageByUid(userId);
    }

    //更改约拍信息为已读
    @RequestMapping("/updateStatusById")
    public Result updateStatusById(String id,String userId) {
        try {
             subYuePaiService.updateStatusById(id,userId);
            return new Result(true,"ok",1);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"ok",0);
        }
    }
}
