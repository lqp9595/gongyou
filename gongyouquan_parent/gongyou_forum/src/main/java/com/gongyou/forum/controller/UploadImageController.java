package com.gongyou.forum.controller;

import com.gongyou.common.utils.DateUtils;
import com.gongyou.common.utils.QiniuUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.UUID;

/**
 * @Author LiQuanPing
 * @Date 2021-04-09 20:24
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
@Slf4j
@RestController
@RequestMapping("/upload")
public class UploadImageController {

    //上传圈子图片到七牛云
    @RequestMapping("/uploadCircleImg")
    public String uploadCircleImg(HttpServletRequest request, @RequestParam("file") MultipartFile[] files) {
        String currentTime = DateUtils.getCurrentTime("yyyyMMddHHmmssSSS");
        String resultName = "";
        for (MultipartFile file : files) {
            String filename = file.getOriginalFilename();//获取原始文件名

            log.info("原始文件名" + filename);
            //获取后缀
            int index = filename.lastIndexOf(".");
            String name = filename.substring(index - 1);
//        使用工具进行文件名拼接
            String name1 = currentTime+"circle-"+UUID.randomUUID().toString()+ name;
//          使用七牛云工具上传到七牛云服务器
            try {
                QiniuUtils.upload2Qiniu(file.getBytes(), name1);
                //执行成功不报错,就保存到redis上面
                // jedisPool.getResource().sadd(RedisConstant.SETMEAL_PIC_RESOURCES,name1);
                resultName = "http://mrliblog.work/" + name1;

            } catch (IOException e) {
                e.printStackTrace();
                // return new Result(false,MessageConstant.PIC_UPLOAD_FAIL);
            }
        }

        return resultName;
    }

    //上传约拍图片到七牛云
    @RequestMapping("/uploadYuepaiImg")
    public String uploadYuepaiImg(HttpServletRequest request, @RequestParam("file") MultipartFile[] files) {
        String currentTime = DateUtils.getCurrentTime("yyyyMMddHHmmssSSS");
        String resultName = "";
        for (MultipartFile file : files) {
            String filename = file.getOriginalFilename();//获取原始文件名

            log.info("原始文件名" + filename);
            //获取后缀
            int index = filename.lastIndexOf(".");
            String name = filename.substring(index - 1);
//        使用工具进行文件名拼接
            String name1 = currentTime+"yuepai-"+UUID.randomUUID().toString()+ name;
//          使用七牛云工具上传到七牛云服务器
            try {
                QiniuUtils.upload2Qiniu(file.getBytes(), name1);
                //执行成功不报错,就保存到redis上面
                // jedisPool.getResource().sadd(RedisConstant.SETMEAL_PIC_RESOURCES,name1);
                resultName = "http://mrliblog.work/" + name1;

            } catch (IOException e) {
                e.printStackTrace();
                // return new Result(false,MessageConstant.PIC_UPLOAD_FAIL);
            }
        }

        return resultName;
    }


    //上传作品图片到七牛云
    @RequestMapping("/uploadZuopinImg")
    public String uploadZuopinImg(HttpServletRequest request, @RequestParam("file") MultipartFile[] files) {
        String currentTime = DateUtils.getCurrentTime("yyyyMMddHHmmssSSS");
        String resultName = "";
        for (MultipartFile file : files) {
            String filename = file.getOriginalFilename();//获取原始文件名

            log.info("原始文件名" + filename);
            //获取后缀
            int index = filename.lastIndexOf(".");
            String name = filename.substring(index - 1);
//        使用工具进行文件名拼接
            String name1 = currentTime+"zuopin-"+UUID.randomUUID().toString()+ name;
//          使用七牛云工具上传到七牛云服务器
            try {
                QiniuUtils.upload2Qiniu(file.getBytes(), name1);
                //执行成功不报错,就保存到redis上面
                // jedisPool.getResource().sadd(RedisConstant.SETMEAL_PIC_RESOURCES,name1);
                resultName = "http://mrliblog.work/" + name1;

            } catch (IOException e) {
                e.printStackTrace();
                // return new Result(false,MessageConstant.PIC_UPLOAD_FAIL);
            }
        }

        return resultName;
    }


    //上传场地图片到七牛云
    @RequestMapping("/uploadChangdiImg")
    public String uploadChangdiImg(HttpServletRequest request, @RequestParam("file") MultipartFile[] files) {
        String currentTime = DateUtils.getCurrentTime("yyyyMMddHHmmssSSS");
        String resultName = "";
        for (MultipartFile file : files) {
            String filename = file.getOriginalFilename();//获取原始文件名

            log.info("原始文件名" + filename);
            //获取后缀
            int index = filename.lastIndexOf(".");
            String name = filename.substring(index - 1);
//        使用工具进行文件名拼接
            String name1 = currentTime+"changdi-"+UUID.randomUUID().toString()+ name;
//          使用七牛云工具上传到七牛云服务器
            try {
                QiniuUtils.upload2Qiniu(file.getBytes(), name1);
                //执行成功不报错,就保存到redis上面
                // jedisPool.getResource().sadd(RedisConstant.SETMEAL_PIC_RESOURCES,name1);
                resultName = "http://mrliblog.work/" + name1;

            } catch (IOException e) {
                e.printStackTrace();
                // return new Result(false,MessageConstant.PIC_UPLOAD_FAIL);
            }
        }

        return resultName;
    }

}
