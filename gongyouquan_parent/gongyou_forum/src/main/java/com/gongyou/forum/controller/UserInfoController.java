package com.gongyou.forum.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gongyou.common.entity.Result;
import com.gongyou.common.pojo.User;
import com.gongyou.common.vo.UserVo;
import com.gongyou.forum.service.UserInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author LiQuanPing
 * @Date 2021-03-26 9:31
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 * 用户个人信息展示
 */
@Slf4j
@RestController
@RequestMapping("/userInfo")
public class UserInfoController {

    @Autowired
    private UserInfoService userInfoService;

    /**
     * @Description:点击用户头像进入用户页面,初始化查询个人圈子动态数据
     * @Param: [别人用户的id=userId, 自己的id=myId, 页码=currentPageNum]
     * @return: com.baomidou.mybatisplus.extension.plugins.pagination.Page
     */
    @GetMapping("/findPageByUserId")
    public Page findPageByUserId(String userId, String myId, String currentPageNum) {
        //log.info(userId + "=" + myId);
        return userInfoService.findPageByUserId(userId, myId, currentPageNum);
    }
    //读取用户个人信息
    @GetMapping("/findUserInfoById")
    public Result findUserInfoById(String userId) {
        //log.info(userId + "=" + myId);
        UserVo userVo =  userInfoService.findUserInfoById(userId);
        return new Result(true,"ok",userVo);
    }

    //用户签到
    @GetMapping("/qianDaoById")
    public Result qianDaoById(String userId) {
        try {
            userInfoService.qianDaoById(userId);
            return new Result(true,"ok",1);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"error",0);
        }

    }


    //用户更改用户信息
    @PostMapping("/updateUserInfoById")
    public Result updateUserInfoById(@RequestBody User user) {
        try {
            userInfoService.updateUserInfoById(user);
            return new Result(true,"ok",1);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"error",0);
        }

    }

}
