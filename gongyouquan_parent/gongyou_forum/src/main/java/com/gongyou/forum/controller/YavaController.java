package com.gongyou.forum.controller;

import com.gongyou.forum.service.YavaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author LiQuanPing
 * @Date 2021-04-08 14:35
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
@RequestMapping("/yava")
@RestController
public class YavaController {

    @Autowired
    private YavaService yavaService;

    //约拍详情页显示约拍头像
    @RequestMapping("/getYavaById")
    public List getYavaById(String id){
        return yavaService.getYavaById(id);
    }
}
