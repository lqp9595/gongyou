package com.gongyou.forum.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gongyou.common.entity.QueryPageBean;
import com.gongyou.common.entity.Result;
import com.gongyou.common.pojo.Yuepai;
import com.gongyou.common.vo.YuepaiVo;
import com.gongyou.forum.service.YuePaiService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @Author LiQuanPing
 * @Date 2021-03-21 16:53
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
@Slf4j
@RestController
@RequestMapping("/yuePai")
public class YuePaiController {

    @Autowired
    private YuePaiService yuePaiService;

    //获取所有约拍列表信息
    @RequestMapping("/getAllYuePaiInfo")
    public Page getAllYuePaiInfo(@RequestBody QueryPageBean queryPageBean) {
//        System.out.println(page);
       return  yuePaiService.getAllYuePaiInfo(queryPageBean);

    }
    //增加约拍浏览量
    @RequestMapping("/updateYuePaiBrowse")
    public void updateYuePaiBrowse(String id) {
         yuePaiService.updateYuePaiBrowse(id);
    }

    //读取用户是否收藏该约拍
    @RequestMapping("/isScYuePaiById")
    public YuepaiVo isScYuePaiById(String id, String uid) {
       return yuePaiService.isScYuePaiById(id,uid);
    }

    //保存发布约拍的信息
    @RequestMapping("/saveYuepaiInfo")
    public Result saveYuepaiInfo(@RequestBody Yuepai yuepai) {
        try {
            yuePaiService.saveYuepaiInfo(yuepai);
            return new Result(true,"ok",1);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"ok",0);
        }
    }


    //读取约拍模特热榜  根据浏览量
    @RequestMapping("/yuePaiModelSelect")
    public Page yuePaiModelSelect(@RequestBody QueryPageBean queryPageBean) {
        return yuePaiService.yuePaiModelSelect(queryPageBean);
    }

    //根据uid获取约拍列表信息
    @RequestMapping("/getYuepaiById")
    public Page getYuepaiById(String uid,String currentPage) {
        return  yuePaiService.getYuepaiById(uid,currentPage);

    }
    //根据uid获取约拍列表信息
    @RequestMapping("/getYuepaiInfoById")
    public List getYuepaiById(String uid) {
        return  yuePaiService.getYuepaiById(uid);

    }

    //根据id删除约拍信息
    @RequestMapping("/delYuepaiById")
    public void delYuepaiById(String id) {
          yuePaiService.delYuepaiById(id);
    }


    //读取我的收藏列表
    @RequestMapping("/getYuepaiByShoucangYId")
    public List getYuepaiByShoucangYId(String userId) {
        return yuePaiService.getYuepaiByShoucangYId(userId);
    }
}
