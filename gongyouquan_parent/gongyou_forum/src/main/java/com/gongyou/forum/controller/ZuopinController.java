package com.gongyou.forum.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gongyou.common.entity.Result;
import com.gongyou.common.pojo.Zuopin;
import com.gongyou.forum.service.ZuopinService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author LiQuanPing
 * @Date 2021-04-12 9:58
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
@RestController
@RequestMapping("/zuopin")
public class ZuopinController {


    @Autowired
    private ZuopinService zuopinService;

    //读取所有作品列表
    @RequestMapping("/getZuopinAll")
    public List getZuopinAll(){
        return zuopinService.getZuopinAll();
    }

    //根据作品id读取作品详情
    @RequestMapping("/getZuopinDetail")
    public Zuopin getZuopinDetail(String id){
        return zuopinService.getZuopinDetail(id);
    }

    //根据作品id增加作品浏览量
    @RequestMapping("/addZuopinView")
    public void addZuopinView(String id){
         zuopinService.addZuopinView(id);
    }
    //根据作品用户id读取所有的作品列表
    @RequestMapping("/getZuopinById")
    public Page getZuopinById(String uid,String currentPage){
        return zuopinService.getZuopinById(uid,currentPage);
    }

    //根据作品用户id读取所有的作品列表
    @RequestMapping("/getZuopinInfoById")
    public List getZuopinById(String uid){
        return zuopinService.getZuopinById(uid);
    }

    //根据id删除约拍信息
    @RequestMapping("/delZuopinById")
    public void delZuopinById(String id) {
        zuopinService.delZuopinById(id);

    }

    //发布作品相册，保存信息
    @RequestMapping("/pulishZuopin")
    public Result pulishZuopin(@RequestBody Zuopin zuopin) {
        try {
            zuopinService.pulishZuopin(zuopin);
            return new Result(true, "ok",1);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "error",0);
        }

    }

}
