package com.gongyou.forum.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gongyou.common.pojo.Changdi;

/**
 * @Author LiQuanPing
 * @Date 2021-04-12 19:42
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
public interface ChangdiMapper extends BaseMapper<Changdi> {
}
