package com.gongyou.forum.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gongyou.common.pojo.Topic;
import com.gongyou.common.vo.CircleVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


/**
 * @Author LiQuanPing
 * @Date 2021-03-19 16:41
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
public interface CircleMapper extends BaseMapper<Topic> {


    Page<CircleVo> getCircle(Page page, @Param("userId") String userId);

    Page<CircleVo> getFollow(Page page, @Param("userId") String userId);

    void insertReplyInfo(@Param("tId") Long tId);

    void insertReplysInfo(@Param("tId") Long tId);


    void updateCirclrComnumByTopicId(long parseLong);

    void updateLikeNum(Long tId);

    void cancelLikeNum(Long tId);

    List<CircleVo> getCircleByUserId(String userId);

    List<CircleVo> getTopicByShoucangId(Long id);
}
