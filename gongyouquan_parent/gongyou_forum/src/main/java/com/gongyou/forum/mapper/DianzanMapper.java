package com.gongyou.forum.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gongyou.common.pojo.Dianzan;

/**
 * @Author LiQuanPing
 * @Date 2021-03-31 20:48
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
public interface DianzanMapper extends BaseMapper<Dianzan> {
}
