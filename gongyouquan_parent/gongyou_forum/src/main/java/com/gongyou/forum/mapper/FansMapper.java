package com.gongyou.forum.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gongyou.common.pojo.Fans;

/**
 * @Author LiQuanPing
 * @Date 2021-04-09 16:50
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
public interface FansMapper extends BaseMapper<Fans> {
}
