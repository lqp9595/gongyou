package com.gongyou.forum.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gongyou.common.pojo.Follow;

/**
 * @Author LiQuanPing
 * @Date 2021-03-26 14:28
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
public interface FollowMapper extends BaseMapper<Follow> {
}
