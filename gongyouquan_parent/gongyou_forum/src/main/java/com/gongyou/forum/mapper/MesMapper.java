package com.gongyou.forum.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gongyou.common.pojo.TMes;

/**
 * @Author LiQuanPing
 * @Date 2021-03-30 13:39
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
public interface MesMapper extends BaseMapper<TMes> {
}
