package com.gongyou.forum.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gongyou.common.pojo.Replys;

/**
 * @Author LiQuanPing
 * @Date 2021-03-30 10:58
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
public interface ReplysMapper extends BaseMapper<Replys> {
}
