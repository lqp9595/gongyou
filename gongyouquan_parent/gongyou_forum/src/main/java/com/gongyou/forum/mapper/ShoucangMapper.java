package com.gongyou.forum.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gongyou.common.pojo.Shoucang;

/**
 * @Author LiQuanPing
 * @Date 2021-03-30 17:16
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
public interface ShoucangMapper extends BaseMapper<Shoucang> {
}
