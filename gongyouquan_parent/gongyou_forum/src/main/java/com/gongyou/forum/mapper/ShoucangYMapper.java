package com.gongyou.forum.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gongyou.common.pojo.Shoucangy;

/**
 * @Author LiQuanPing
 * @Date 2021-04-09 15:49
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
public interface ShoucangYMapper extends BaseMapper<Shoucangy> {
}
