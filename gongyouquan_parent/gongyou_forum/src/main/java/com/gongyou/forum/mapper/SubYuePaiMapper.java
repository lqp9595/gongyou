package com.gongyou.forum.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gongyou.common.pojo.Suby;

/**
 * @Author LiQuanPing
 * @Date 2021-04-08 15:37
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
public interface SubYuePaiMapper extends BaseMapper<Suby> {
}
