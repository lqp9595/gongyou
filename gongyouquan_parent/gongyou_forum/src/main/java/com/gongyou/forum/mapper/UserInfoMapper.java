package com.gongyou.forum.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gongyou.common.pojo.User;
import com.gongyou.common.vo.CircleVo;
import org.apache.ibatis.annotations.Param;

/**
 * @Author LiQuanPing
 * @Date 2021-03-26 11:38
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
public interface UserInfoMapper extends BaseMapper<User> {
    Page<CircleVo> findPageByUserId(Page page, @Param("userId") String userId, @Param("myId") String myId, @Param("currentPageNum") String currentPageNum);
}
