package com.gongyou.forum.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gongyou.common.pojo.User;

/**
 * @Author LiQuanPing
 * @Date 2021-03-23 16:22
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
public interface UserLoginMapper extends BaseMapper<User> {
}
