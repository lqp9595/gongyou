package com.gongyou.forum.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gongyou.common.pojo.Yuepai;
import com.gongyou.common.vo.YuepaiVo;
import org.apache.ibatis.annotations.Param;


import java.util.List;

/**
 * @Author LiQuanPing
 * @Date 2021-03-21 16:55
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
public interface YuePaiMapper extends BaseMapper<Yuepai> {


    YuepaiVo isScYuePaiById(@Param("id") String id, @Param("uid") String uid);

    List<Yuepai> getYuepaiByShoucangYId(long id);

}
