package com.gongyou.forum.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gongyou.common.pojo.Zuopin;

/**
 * @Author LiQuanPing
 * @Date 2021-04-12 9:59
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
public interface ZuopinMapper extends BaseMapper<Zuopin> {

}
