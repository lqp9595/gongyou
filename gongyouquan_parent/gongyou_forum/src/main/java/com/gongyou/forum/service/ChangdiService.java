package com.gongyou.forum.service;

import com.gongyou.common.pojo.Changdi;

import java.util.List;

/**
 * @Author LiQuanPing
 * @Date 2021-04-12 19:43
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
public interface ChangdiService {
    List getChangdiAll();


    Changdi getChangdiDetail(String id);

    void addChangdiView(String id);

    void pulishChangdi(Changdi changdi);

}
