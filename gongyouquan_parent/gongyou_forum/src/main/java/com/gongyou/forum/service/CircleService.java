package com.gongyou.forum.service;



import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gongyou.common.pojo.Topic;

import java.util.List;

/**
 * @Author LiQuanPing
 * @Date 2021-03-19 16:40
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
public interface CircleService {
    Page getCircle(String userId,String pageNum);




    void publishCircle(Topic topic);

    Page getFollow(String userId, String pageNum);

    List getCommentById(String id);

    List getCircleByUserId(String userId);

    void delTopicById(String tid);

    List getTopicByShoucangId(String userId);

}
