package com.gongyou.forum.service;

import com.gongyou.common.pojo.Dianzan;

/**
 * @Author LiQuanPing
 * @Date 2021-03-31 20:48
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
public interface DianzanService {
    void addDianzan(Dianzan dianzan);

    void cancelDianzan(Dianzan dianzan);
}
