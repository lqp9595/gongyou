package com.gongyou.forum.service;

import java.util.List;
import java.util.Map;

/**
 * @Author LiQuanPing
 * @Date 2021-03-26 14:27
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
public interface FollowService {
    boolean isFollowByUserId(String userId, String myId);

    void FollowYuePaiById(Map map);

    void cancelFollowYuePaiById(String uid, String fid);

    List findFansById(String userId);

    List findFollowByid(String userId);
}
