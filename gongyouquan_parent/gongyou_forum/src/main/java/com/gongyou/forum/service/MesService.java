package com.gongyou.forum.service;

import java.util.List;

/**
 * @Author LiQuanPing
 * @Date 2021-03-30 13:40
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
public interface MesService {
    List findMesById(String id);
}
