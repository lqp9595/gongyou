package com.gongyou.forum.service;

import com.gongyou.common.pojo.Reply;

import java.util.List;

/**
 * @Author LiQuanPing
 * @Date 2021-03-30 10:09
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
public interface ReplyService {
    List getReplyByTopicId(String topicId);

    void insertReplyInfo(Reply reply, String topicUserId);

    void deleteReplyAndMes(String id, String tid, String time);

}
