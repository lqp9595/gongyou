package com.gongyou.forum.service;

import com.gongyou.common.pojo.Replys;

import java.util.List;

/**
 * @Author LiQuanPing
 * @Date 2021-03-30 10:58
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
public interface ReplysService {
    List getReplyByTopicId(String topicId);

    void insertReplysInfo(Replys replys, String topicUserId);

    void deleteReplysAndMes(String id, String tid, String time);
}
