package com.gongyou.forum.service;

import com.gongyou.common.pojo.Shoucang;

/**
 * @Author LiQuanPing
 * @Date 2021-03-30 17:17
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
public interface ShoucangService {
    void addSc(Shoucang shoucang);

    void deleteSc(Shoucang shoucang);
}
