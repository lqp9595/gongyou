package com.gongyou.forum.service;

/**
 * @Author LiQuanPing
 * @Date 2021-04-09 15:48
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
public interface ShoucangYService {
    void addShoucangY(String yId, String userId);

    void cancelShoucangY(String yId, String userId);
}
