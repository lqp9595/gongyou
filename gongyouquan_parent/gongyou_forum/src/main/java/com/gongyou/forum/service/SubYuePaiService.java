package com.gongyou.forum.service;

import com.gongyou.common.pojo.Suby;

import java.util.List;

/**
 * @Author LiQuanPing
 * @Date 2021-04-08 15:38
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
public interface SubYuePaiService {
    void submitYuePai(Suby suby);

    List getYuePaiMessage(String userId);

    List getYuePaiMessageByUid(String userId);


    void updateStatusById(String id, String userId);
}
