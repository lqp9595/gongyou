package com.gongyou.forum.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gongyou.common.pojo.User;
import com.gongyou.common.vo.UserVo;

/**
 * @Author LiQuanPing
 * @Date 2021-03-26 11:38
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
public interface UserInfoService {
    Page findPageByUserId(String userId, String myId, String currentPageNum);

    UserVo findUserInfoById(String userId);

    void qianDaoById(String userId);

    void updateUserInfoById(User user);
}
