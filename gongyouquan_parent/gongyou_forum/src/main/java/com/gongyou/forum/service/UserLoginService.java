package com.gongyou.forum.service;

import com.gongyou.common.entity.Result;
import com.gongyou.common.pojo.User;

import java.io.IOException;

/**
 * @Author LiQuanPing
 * @Date 2021-03-23 16:21
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
public interface UserLoginService {
    User login(User user);

    void register(User user) throws IOException;

    boolean phoneIsReg(String phone);

    void editPasswordByPhone(User user);

    Result weChatLogin(String code);
}
