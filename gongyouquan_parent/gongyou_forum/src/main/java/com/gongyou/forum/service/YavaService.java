package com.gongyou.forum.service;

import java.util.List;

/**
 * @Author LiQuanPing
 * @Date 2021-04-08 14:35
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
public interface YavaService {
    List getYavaById(String id);
}
