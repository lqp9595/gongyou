package com.gongyou.forum.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gongyou.common.entity.QueryPageBean;
import com.gongyou.common.pojo.Yuepai;
import com.gongyou.common.vo.YuepaiVo;

import java.util.List;
import java.util.Map;

/**
 * @Author LiQuanPing
 * @Date 2021-03-21 16:54
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
public interface YuePaiService {
    Page getAllYuePaiInfo(QueryPageBean queryPageBean);

    void updateYuePaiBrowse(String id);

    YuepaiVo isScYuePaiById(String id, String uid);

    void saveYuepaiInfo(Yuepai yuepai);

    Page yuePaiModelSelect(QueryPageBean queryPageBean);

    Page getYuepaiById(String uid, String currentPage);

    List getYuepaiById(String uid);

    void delYuepaiById(String id);

    List getYuepaiByShoucangYId(String userId);

}
