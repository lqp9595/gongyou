package com.gongyou.forum.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gongyou.common.pojo.Zuopin;

import java.util.List;

/**
 * @Author LiQuanPing
 * @Date 2021-04-12 9:59
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
public interface ZuopinService {
    List getZuopinAll();


    Zuopin getZuopinDetail(String id);

    void addZuopinView(String id);

    Page getZuopinById(String uid, String currentPage);

    List getZuopinById(String uid);

    void delZuopinById(String id);

    void pulishZuopin(Zuopin zuopin);

}
