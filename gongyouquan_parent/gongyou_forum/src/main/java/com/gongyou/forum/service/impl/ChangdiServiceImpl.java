package com.gongyou.forum.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gongyou.common.pojo.Changdi;
import com.gongyou.forum.mapper.ChangdiMapper;
import com.gongyou.forum.service.ChangdiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author LiQuanPing
 * @Date 2021-04-12 19:43
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
@Service
public class ChangdiServiceImpl implements ChangdiService {

    @Autowired
    private ChangdiMapper changdiMapper;

    //获取所有场地列表
    @Override
    public List getChangdiAll() {
        List<Changdi> changdis = changdiMapper.selectList(new QueryWrapper<Changdi>().eq("is_del", 0).orderByDesc("pub_date"));
        for (Changdi changdi : changdis) {
            List list = JSON.parseObject(changdi.getUrls(), List.class);
            changdi.setUrlsList(list);
        }
        return changdis;
    }

    //获取id获取场地详情
    @Override
    public Changdi getChangdiDetail(String id) {
        Changdi changdi = changdiMapper.selectById(id);
        List list1 = JSON.parseObject(changdi.getCTags(), List.class);
        changdi.setCtagsList(list1);
        List list = JSON.parseObject(changdi.getUrls(), List.class);
        changdi.setUrlsList(list);
        return changdi;
    }

    //增加场地浏览量
    @Override
    public void addChangdiView(String id) {
        Changdi changdi = changdiMapper.selectById(id);
        changdi.setView(changdi.getView()+1);
        changdiMapper.updateById(changdi);
    }

    //发布场地相册 保存信息
    @Override
    public void pulishChangdi(Changdi changdi) {
        changdiMapper.insert(changdi);
    }
}
