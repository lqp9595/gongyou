package com.gongyou.forum.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gongyou.common.pojo.Shoucang;
import com.gongyou.common.pojo.Topic;
import com.gongyou.common.vo.CircleVo;
import com.gongyou.forum.mapper.CircleMapper;
import com.gongyou.forum.mapper.ShoucangMapper;
import com.gongyou.forum.service.CircleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @Author LiQuanPing
 * @Date 2021-03-19 16:40
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
@Service
public class CircleServiceImpl implements CircleService {

    @Autowired
    private CircleMapper circleMapper;

    @Autowired
    private ShoucangMapper shoucangMapper;
    //读取圈子所有列表
    @Override
    public Page getCircle(String userId, String pageNum) {
        long size = Long.parseLong(pageNum);
        Page page = new Page(0,size*10);
       // List<Map> list = circleMapper.getCircle(page,userId);
        Page<CircleVo> page1 = circleMapper.getCircle(page,userId);
        List<CircleVo> records = page1.getRecords();
        for (CircleVo topic : records) {
            String url = topic.getUrl();
            List list1 = JSON.parseObject(url, List.class);
            topic.setPocUrl(list1);
         /*   String reqDate = DateUtils.mill2String(topic.getRepDate());
            String pubDate = DateUtils.mill2String(topic.getPubDate());
            topic.setRepDate(reqDate);
            topic.setPubDate(pubDate);*/
        }
        // System.out.println("===="+list);
       /* List<Map> records = page1.getRecords();
        List<CircleVo> circleList = new ArrayList();
        for (Map map : records) {
            CircleVo circleVo = JSON.parseObject(JSON.toJSONString(map), CircleVo.class);
            circleList.add(circleVo);
        }
          return circleList;
        */
       page1.setRecords(records);
      return page1;
    }

    //读取我关注的圈子所有列表
    @Override
    public Page getFollow(String userId, String pageNum) {
        long size = Long.parseLong(pageNum);
        Page page = new Page(0,size*10);
        Page<CircleVo> page1 = circleMapper.getFollow(page,userId);
        List<CircleVo> records = page1.getRecords();
        for (CircleVo topic : records) {
            String url = topic.getUrl();
            List list1 = JSON.parseObject(url, List.class);
            topic.setPocUrl(list1);
        }
        page1.setRecords(records);
        return page1;
    }

    //根据id获取图片信息,用于展示评论和回复用
    @Override
    public List getCommentById(String id) {
        Topic topic = circleMapper.selectById(id);
        String url = topic.getUrl();
        List list = JSON.parseObject(url, List.class);
        topic.setPocUrl(list);
        List resultList = new ArrayList();
        resultList.add(topic);
        return resultList;
    }

    //发布圈子动态,保存发布的信息
    @Override
    public void publishCircle(Topic topic) {
        List pocUrl = topic.getPocUrl();
        String url = JSON.toJSONString(pocUrl);
        topic.setUrl(url);
        int insert = circleMapper.insert(topic);
    //    System.out.println(insert + "]]]]]]]]]]");
    }

    //根据userId读取我的圈子所有列表
    @Override
    public List getCircleByUserId(String userId) {
         List<CircleVo> list = circleMapper.getCircleByUserId(userId);
        for (CircleVo circleVo : list) {
            String url = circleVo.getUrl();
            List list1 = JSON.parseObject(url, List.class);
            circleVo.setPocUrl(list1);
        }
         return list;
    }

    //根据id删除topic圈子信息和收藏
    @Override
    public void delTopicById(String tid) {
        Topic topic = new Topic();
        topic.setIsDel(1);
        topic.setId(Long.parseLong(tid));
        circleMapper.updateById(topic);

        shoucangMapper.delete(new QueryWrapper<Shoucang>().eq("t_id",tid));
    }

    //读取我收藏的topic列表
    @Override
    public List getTopicByShoucangId(String userId) {
        long id = Long.parseLong(userId);
        List<CircleVo> list =  circleMapper.getTopicByShoucangId(id);
        for (CircleVo circleVo : list) {
            List list1 = JSON.parseObject(circleVo.getUrl(), List.class);
            circleVo.setPocUrl(list1);
        }
        return list;
    }
}
