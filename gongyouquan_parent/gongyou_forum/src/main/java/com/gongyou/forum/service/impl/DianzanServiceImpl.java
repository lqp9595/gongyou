package com.gongyou.forum.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gongyou.common.pojo.Dianzan;
import com.gongyou.forum.mapper.CircleMapper;
import com.gongyou.forum.mapper.DianzanMapper;
import com.gongyou.forum.service.DianzanService;
import com.netflix.discovery.converters.Auto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Author LiQuanPing
 * @Date 2021-03-31 20:48
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
@Service
public class DianzanServiceImpl implements DianzanService {

    @Autowired
    private DianzanMapper dianzanMapper;


    @Autowired
    private CircleMapper circleMapper;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    //点赞
    @Override
    @Transactional(readOnly = false)
    public void addDianzan(Dianzan dianzan) {
//        stringRedisTemplate.opsForValue()
        Long tId = dianzan.getTId();
        circleMapper.updateLikeNum(tId);//当前图片点赞数加一
        dianzanMapper.insert(dianzan);//插入点赞记录表
    }

    //取消点赞
    @Override
    @Transactional(readOnly = false)
    public void cancelDianzan(Dianzan dianzan) {
        Long tId = dianzan.getTId();
        circleMapper.cancelLikeNum(tId);
        dianzanMapper.delete(new QueryWrapper<Dianzan>().eq("t_id",tId).eq("u_id",dianzan.getUId()));
    }



}
