package com.gongyou.forum.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gongyou.common.pojo.Fans;
import com.gongyou.common.pojo.Follow;
import com.gongyou.common.pojo.User;
import com.gongyou.forum.mapper.FansMapper;
import com.gongyou.forum.mapper.FollowMapper;
import com.gongyou.forum.mapper.UserInfoMapper;
import com.gongyou.forum.service.FollowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Author LiQuanPing
 * @Date 2021-03-26 14:27
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
@Service
public class FollowServiceImpl implements FollowService {

    @Autowired
    private FollowMapper followMapper;

    @Autowired
    private UserInfoMapper userInfoMapper;

    @Autowired
    private FansMapper fansMapper;

    @Override
    public boolean isFollowByUserId(String userId, String myId) {
        List<Follow> follows = followMapper.selectList(new QueryWrapper<Follow>().eq("u_id", myId).eq("f_id", userId));
        if (follows.size() <= 0 || follows == null) {
            return false;
        } else {
            return true;
        }
    }

    //关注约拍用户
    @Override
    public void FollowYuePaiById(Map map) {
        String uid = map.get("uid") + "";
        String uava = (String) map.get("uava");
        String uname = (String) map.get("uname");
        String ugender = (String) map.get("ugender");
        String fid = map.get("fid") + "";
        String fava = (String) map.get("fava");
        String fname = (String) map.get("fname");
        String fgender = (String) map.get("fgender");
        long uidL = Long.parseLong(uid);
        long fidL = Long.parseLong(fid);
        //添加到关注表
        Follow follow = new Follow();
        follow.setUId(uidL);
        follow.setFId(fidL);
        follow.setFAva(fava);
        follow.setFName(fname);
        follow.setFGender(fgender);
        followMapper.insert(follow);
        //添加到粉丝表
        Fans fans = new Fans();
        fans.setUId(fidL);
        fans.setFId(uidL);
        fans.setFAva(uava);
        fans.setFName(uname);
        fans.setFGender(ugender);
        fansMapper.insert(fans);
        //关注者关注数加一
        User user = userInfoMapper.selectById(uid);
        user.setFollow(user.getFollow() + 1);
        userInfoMapper.updateById(user);
        //被关注者粉丝数量加一
        User user1 = userInfoMapper.selectById(fid);
        user1.setFans(user1.getFans() + 1);
        userInfoMapper.updateById(user1);
    }

    //取消关注约拍用户
    @Override
    public void cancelFollowYuePaiById(String uid, String fid) {
        Long uidL = Long.parseLong(uid);
        Long fidL = Long.parseLong(fid);
        //删除关注表
        followMapper.delete(new QueryWrapper<Follow>().eq("u_id", uidL).eq("f_id", fidL));
        //删除粉丝表中记录
        fansMapper.delete(new QueryWrapper<Fans>().eq("u_id", fidL).eq("f_id", uidL));
        //关注者关注数减一
        User user = userInfoMapper.selectById(uid);
        user.setFollow(user.getFollow() - 1);
        userInfoMapper.updateById(user);
        //被关注者粉丝减一
        User user1 = userInfoMapper.selectById(fid);
        user1.setFans(user1.getFans() - 1);
        userInfoMapper.updateById(user1);

    }

    //根据id查找用户的粉丝
    @Override
    public List findFansById(String userId) {
        return fansMapper.selectList(new QueryWrapper<Fans>().eq("u_id",userId));
    }

    //根据id查找用户的关注
    @Override
    public List findFollowByid(String userId) {
        return followMapper.selectList(new QueryWrapper<Follow>().eq("u_id",userId));
    }
}
