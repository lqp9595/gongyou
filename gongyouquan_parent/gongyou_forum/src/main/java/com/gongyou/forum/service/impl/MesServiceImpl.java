package com.gongyou.forum.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gongyou.common.pojo.TMes;
import com.gongyou.forum.mapper.MesMapper;
import com.gongyou.forum.service.MesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author LiQuanPing
 * @Date 2021-03-30 13:40
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
@Service
public class MesServiceImpl implements MesService {

    @Autowired
    private MesMapper mesMapper;
    @Override
    public List findMesById(String id) {
        return mesMapper.selectList(new QueryWrapper<TMes>().eq("u_id",id).eq("is_del",0).orderByDesc("date"));
    }
}
