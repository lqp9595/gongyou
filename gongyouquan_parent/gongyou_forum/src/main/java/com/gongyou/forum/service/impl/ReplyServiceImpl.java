package com.gongyou.forum.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gongyou.common.pojo.Reply;
import com.gongyou.common.pojo.TMes;
import com.gongyou.common.utils.DateUtils;
import com.gongyou.forum.mapper.CircleMapper;
import com.gongyou.forum.mapper.MesMapper;
import com.gongyou.forum.mapper.ReplyMapper;
import com.gongyou.forum.service.ReplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author LiQuanPing
 * @Date 2021-03-30 10:09
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
@Service
public class ReplyServiceImpl implements ReplyService {

    @Autowired
    private ReplyMapper replyMapper;

    @Autowired
    private CircleMapper circleMapper;

    @Autowired
    private MesMapper mesMapper;
    //读取回复
    @Override
    public List getReplyByTopicId(String topicId) {
        return replyMapper.selectList(new QueryWrapper<Reply>().eq("t_id",topicId).eq("is_del",0).orderByDesc("r_date"));
    }

    //对当前发图片的用户发表评论
    @Override
    public void insertReplyInfo(Reply reply,String topicUserId) {
        Long tId = reply.getTId();
     //   String currentTime = DateUtils.getCurrentTime("yyyy-MM-dd HH:mm:ss");
        //对当前发表的信息评论数加一,并且修改最新发布日期
        circleMapper.insertReplyInfo(tId);
        //插入评论表reply
        replyMapper.insert(reply);
        //插入消息表
        TMes mes = new TMes();
        mes.setTId(tId);//图片id
        mes.setName(reply.getRName());
        mes.setContent(reply.getRCont());
        mes.setUAva(reply.getRAva());
        mes.setUId(Long.parseLong(topicUserId));
        mesMapper.insert(mes);
    }

    //删除评论信息
    @Override
    public void deleteReplyAndMes(String id, String tid, String time) {

        //先对圈子表中评论数量减一
        circleMapper.updateCirclrComnumByTopicId(Long.parseLong(tid));
        //删除reply表中评论数据
       // replyMapper.deleteReplyById(id);
        Reply reply = new Reply();
        reply.setIsDel(1);
        replyMapper.update(reply,new QueryWrapper<Reply>().eq("id",id));
        //删除信息表中数据
       // mesMapper.deleteMesByTime(time);
        TMes mes = new TMes();
        mes.setIsDel(1);
        mesMapper.update(mes,new QueryWrapper<TMes>().eq("date",time));
    }
}
