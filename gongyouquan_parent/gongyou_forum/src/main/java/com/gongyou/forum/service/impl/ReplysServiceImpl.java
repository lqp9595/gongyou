package com.gongyou.forum.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gongyou.common.pojo.Replys;
import com.gongyou.common.pojo.TMes;
import com.gongyou.forum.mapper.CircleMapper;
import com.gongyou.forum.mapper.MesMapper;
import com.gongyou.forum.mapper.ReplysMapper;
import com.gongyou.forum.service.ReplysService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author LiQuanPing
 * @Date 2021-03-30 10:58
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
@Service
public class ReplysServiceImpl implements ReplysService {

    @Autowired
    private ReplysMapper replysMapper;

    @Autowired
    private CircleMapper circleMapper;

    @Autowired
    private MesMapper mesMapper;

    //根据图片id读取多级回复信息
    @Override
    public List getReplyByTopicId(String topicId) {
        return replysMapper.selectList(new QueryWrapper<Replys>().eq("t_id", topicId).eq("is_del",0).orderByDesc("r_date"));
    }

    //对当前发图片的用户发表多级评论
    @Override
    public void insertReplysInfo(Replys replys, String topicUserId) {

        Long tId = replys.getTId();//图片id
        //对回复数加一  并修改最新回复时间
        circleMapper.insertReplysInfo(tId);
        //插入多级回复表
        replysMapper.insert(replys);
        //插入信息表
        TMes mes = new TMes();
        mes.setTId(tId);//图片id
        mes.setName(replys.getName());
        mes.setContent(replys.getContent());
        mes.setUAva(replys.getUAva());
        mes.setUId(Long.parseLong(topicUserId));
        mesMapper.insert(mes);
    }

    //删除多级评论信息
    @Override
    public void deleteReplysAndMes(String id, String tid, String time) {

        //先对圈子表中评论数量减一
        circleMapper.updateCirclrComnumByTopicId(Long.parseLong(tid));
        //删除reply表中评论数据
        // replyMapper.deleteReplyById(id);
        Replys replys = new Replys();
        replys.setIsDel(1);
        replysMapper.update(replys,new QueryWrapper<Replys>().eq("id",id));
        //删除信息表中数据
        // mesMapper.deleteMesByTime(time);
        TMes mes = new TMes();
        mes.setIsDel(1);
        mesMapper.update(mes,new QueryWrapper<TMes>().eq("date",time));
    }
}
