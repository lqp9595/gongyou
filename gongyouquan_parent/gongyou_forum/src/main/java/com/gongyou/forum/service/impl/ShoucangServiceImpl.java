package com.gongyou.forum.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gongyou.common.pojo.Shoucang;
import com.gongyou.forum.mapper.ShoucangMapper;
import com.gongyou.forum.service.ShoucangService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author LiQuanPing
 * @Date 2021-03-30 17:17
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
@Service
public class ShoucangServiceImpl implements ShoucangService {

    @Autowired
    private ShoucangMapper shoucangMapper;

    //添加收藏
    @Override
    public void addSc(Shoucang shoucang) {
        shoucangMapper.insert(shoucang);
    }

    //取消收藏
    @Override
    public void deleteSc(Shoucang shoucang) {
        shoucangMapper.delete(new QueryWrapper<Shoucang>().eq("t_id",shoucang.getTId()).eq("u_id",shoucang.getUId()));
    }
}
