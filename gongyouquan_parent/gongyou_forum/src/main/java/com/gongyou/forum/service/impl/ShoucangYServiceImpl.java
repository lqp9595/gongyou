package com.gongyou.forum.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gongyou.common.pojo.Shoucangy;
import com.gongyou.forum.mapper.ShoucangYMapper;
import com.gongyou.forum.service.ShoucangYService;
import com.gongyou.forum.service.YavaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author LiQuanPing
 * @Date 2021-04-09 15:49
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
@Service
public class ShoucangYServiceImpl implements ShoucangYService {

    @Autowired
    private ShoucangYMapper shoucangYMapper;

    @Override
    public void addShoucangY(String yId, String userId) {
        Shoucangy shoucangy = new Shoucangy();
        shoucangy.setYId(Long.parseLong(yId));
        shoucangy.setUId(Long.parseLong(userId));
        shoucangYMapper.insert(shoucangy);

    }

    @Override
    public void cancelShoucangY(String yId, String userId) {
        shoucangYMapper.delete(new QueryWrapper<Shoucangy>().eq("y_id",yId).eq("u_id",userId));
    }
}
