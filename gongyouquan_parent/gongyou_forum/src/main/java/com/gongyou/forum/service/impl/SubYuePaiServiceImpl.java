package com.gongyou.forum.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gongyou.common.pojo.Suby;
import com.gongyou.common.pojo.User;
import com.gongyou.common.pojo.YAva;
import com.gongyou.common.pojo.Yuepai;
import com.gongyou.forum.mapper.*;
import com.gongyou.forum.service.SubYuePaiService;
import com.netflix.discovery.converters.Auto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author LiQuanPing
 * @Date 2021-04-08 15:38
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
@Service
public class SubYuePaiServiceImpl implements SubYuePaiService {


    @Autowired
    private SubYuePaiMapper subYuePaiMapper;

    @Autowired
    private YavaMapper yavaMapper;

    @Autowired
    private UserLoginMapper userLoginMapper;

    @Autowired
    private YuePaiMapper yuePaiMapper;
    //发布约拍请求
    @Override
    public void submitYuePai(Suby suby) {

        //约拍表中对约拍数量加1
        Yuepai yuepai = yuePaiMapper.selectById(suby.getYId());
        yuepai.setView(yuepai.getYNum()+1);
        yuePaiMapper.updateById(yuepai);
        //用户积分减一
        User user = userLoginMapper.selectById(suby.getMId());
        user.setIntegral(user.getIntegral()-1);
        userLoginMapper.updateById(user);
        //插入发布约拍头像表
        YAva yAva = new YAva();
        yAva.setYId(suby.getYId());
        yAva.setUAva(suby.getMAva());
        yavaMapper.insert(yAva);
        //插入发布约拍信息表
        subYuePaiMapper.insert(suby);


    }

    //读取我发布的约拍信息
    @Override
    public List getYuePaiMessage(String userId) {

        return  subYuePaiMapper.selectList(new QueryWrapper<Suby>().eq("m_id",userId).orderByDesc("date"));

    }
    //读取我收到的约拍信息
    @Override
    public List getYuePaiMessageByUid(String userId) {
        return  subYuePaiMapper.selectList(new QueryWrapper<Suby>().eq("u_id",userId).orderByDesc("date"));

    }

    //更改约拍信息为已读
    @Override
    public void updateStatusById(String id, String userId) {
        //修改状态为已读
        Suby suby = new Suby();
        suby.setId(Long.parseLong(id));
        suby.setStatus(1);
        subYuePaiMapper.updateById(suby);
        //用户积分减一
        User user = userLoginMapper.selectById(userId);
        user.setIntegral(user.getIntegral()-1);
        userLoginMapper.updateById(user);
    }
}
