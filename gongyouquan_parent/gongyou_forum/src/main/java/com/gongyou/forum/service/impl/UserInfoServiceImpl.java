package com.gongyou.forum.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gongyou.common.pojo.User;
import com.gongyou.common.vo.CircleVo;
import com.gongyou.common.vo.UserVo;
import com.gongyou.forum.mapper.UserInfoMapper;
import com.gongyou.forum.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @Author LiQuanPing
 * @Date 2021-03-26 11:38
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
@Service
public class UserInfoServiceImpl implements UserInfoService {

    @Autowired
    private UserInfoMapper userInfoMapper;

    //点击用户头像进入用户页面,初始化查询个人圈子动态数据
    @Override
    public Page findPageByUserId(String userId, String myId, String currentPageNum) {
        long size = Long.parseLong(currentPageNum);
        Page page = new Page(0, size * 10);
        Page<CircleVo> page1 = userInfoMapper.findPageByUserId(page,userId,myId,currentPageNum);
        List<CircleVo> records = page1.getRecords();
        for (CircleVo record : records) {
            String url = record.getUrl();
            List list = JSON.parseObject(url, List.class);
            record.setPocUrl(list);
        }
        page1.setRecords(records);
        return page1;
    }

    @Override
    public UserVo findUserInfoById(String userId) {
        User user = userInfoMapper.selectById(userId);
        UserVo userVo = JSON.parseObject(JSON.toJSONString(user), UserVo.class);
        return userVo;
    }

    //用户签到
    @Override
    public void qianDaoById(String userId) {
        User user = userInfoMapper.selectById(userId);
        user.setIntegral(user.getIntegral()+1);
        user.setStatus(1);
        userInfoMapper.updateById(user);
    }

    //用户修改个人用户信息   这里需要优化，需要更改所有表中的头像
    @Override
    public void updateUserInfoById(User user) {
        userInfoMapper.updateById(user);
    }
}
