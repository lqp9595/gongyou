package com.gongyou.forum.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gongyou.common.pojo.YAva;
import com.gongyou.forum.mapper.YavaMapper;
import com.gongyou.forum.service.YavaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author LiQuanPing
 * @Date 2021-04-08 14:35
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
@Service
public class YavaServiceImpl implements YavaService {

    @Autowired
    private YavaMapper yavaMapper;

    //约拍详情页显示约拍头像
    @Override
    public List getYavaById(String id) {
        return yavaMapper.selectList(new QueryWrapper<YAva>().eq("y_id",id));
    }
}
