package com.gongyou.forum.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gongyou.common.entity.QueryPageBean;
import com.gongyou.common.pojo.Shoucangy;
import com.gongyou.common.pojo.Yuepai;
import com.gongyou.common.vo.YuepaiVo;
import com.gongyou.forum.mapper.ShoucangYMapper;
import com.gongyou.forum.mapper.YuePaiMapper;
import com.gongyou.forum.service.YuePaiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Author LiQuanPing
 * @Date 2021-03-21 16:55
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
@Service
public class YuePaiServiceImpl implements YuePaiService {

    @Autowired
    private YuePaiMapper yuePaiMapper;

    @Autowired
    private ShoucangYMapper shoucangYMapper;

    //读取所有约拍信息
    @Override
    public Page getAllYuePaiInfo(QueryPageBean queryPageBean) {
        String text1 = queryPageBean.getText1();
        String text2 = queryPageBean.getText2();
        String text3 = queryPageBean.getText3();
        Integer currentPage = queryPageBean.getCurrentPage();
        Page page = new Page(0, currentPage * 10);
        Page page1 = yuePaiMapper.selectPage(page, new QueryWrapper<Yuepai>().eq(!text1.equals(""), "identity", text1).eq(!text2.equals(""), "p_gender", text2).eq(!text3.equals(""), "faceplace", text3).eq("is_del", 0).orderByDesc("pub_date"));
        List<Yuepai> records = page1.getRecords();
        if (records.size() > 0 || records != null) {
            for (Yuepai record : records) {
                List list = JSON.parseObject(record.getUrls(), List.class);
                record.setUrlsList(list);
            }
        }
        return page1.setRecords(records);
    }

    //增加约拍浏览量
    @Override
    public void updateYuePaiBrowse(String id) {
        Yuepai yuepai = yuePaiMapper.selectById(id);
        yuepai.setView(yuepai.getView() + 1);
        yuePaiMapper.updateById(yuepai);
    }


    @Override
    public YuepaiVo isScYuePaiById(String id, String uid) {
        YuepaiVo yuepaiVo = yuePaiMapper.isScYuePaiById(id, uid);
        List list1 = JSON.parseObject(yuepaiVo.getPTags(), List.class);
        List list2 = JSON.parseObject(yuepaiVo.getUrls(), List.class);
        yuepaiVo.setPTagsList(list1);
        yuepaiVo.setUrlsList(list2);

        return yuepaiVo;

    }

    //保存发布约拍信息
    @Override
    public void saveYuepaiInfo(Yuepai yuepai) {
        yuePaiMapper.insert(yuepai);
    }

    //读取模特热榜
    @Override
    public Page yuePaiModelSelect(QueryPageBean queryPageBean) {
        String text1 = queryPageBean.getText1();
        String text2 = queryPageBean.getText2();
        Integer currentPage = queryPageBean.getCurrentPage();

        Page page = new Page(0, currentPage * 10);
        Page page1 = yuePaiMapper.selectPage(page, new QueryWrapper<Yuepai>().eq(!text1.equals(""), "identity", text1).eq(!text2.equals(""), "p_gender", text2).eq("is_del", 0).ge("view", 100).orderByDesc("view"));
        List<Yuepai> records = page1.getRecords();
        for (Yuepai record : records) {
            List urlList = JSON.parseObject(record.getUrls(), List.class);
            List tagsList = JSON.parseObject(record.getPTags(), List.class);
            record.setUrlsList(urlList);
            record.setPTagsList(tagsList);
        }
        return page1.setRecords(records);
    }

    //根据id读取约拍信息
    @Override
    public Page getYuepaiById(String uid, String currentPage) {
        long l = Long.parseLong(currentPage);
        Page page = new Page(0, l * 10);
        Page page1 = yuePaiMapper.selectPage(page, new QueryWrapper<Yuepai>().eq("p_id", uid).eq("is_del", 0).orderByDesc("pub_date"));
        List<Yuepai> yuepais = page1.getRecords();
        for (Yuepai yuepai : yuepais) {
            List list = JSON.parseObject(yuepai.getUrls(), List.class);
            yuepai.setUrlsList(list);
            List list1 = JSON.parseObject(yuepai.getPTags(), List.class);
            yuepai.setPTagsList(list1);
        }
        return page1.setRecords(yuepais);
    }

    @Override
    public List getYuepaiById(String uid) {
        List<Yuepai> yuepais = yuePaiMapper.selectList(new QueryWrapper<Yuepai>().eq("p_id", uid).eq("is_del", 0).orderByDesc("pub_date"));
        for (Yuepai yuepai : yuepais) {
            List list = JSON.parseObject(yuepai.getUrls(), List.class);
            yuepai.setUrlsList(list);
            List list1 = JSON.parseObject(yuepai.getPTags(), List.class);
            yuepai.setPTagsList(list1);
        }
        return yuepais;
    }

    //根据id删除约拍和收藏
    @Override
    public void delYuepaiById(String id) {
        Yuepai yuepai = new Yuepai();
        yuepai.setId(Long.parseLong(id));
        yuepai.setIsDel(1);
        yuePaiMapper.updateById(yuepai);

        shoucangYMapper.delete(new QueryWrapper<Shoucangy>().eq("y_id", id));
    }

    //读取我的收藏列表
    @Override
    public List<Yuepai> getYuepaiByShoucangYId(String userId) {
        long id = Long.parseLong(userId);
        List<Yuepai> list = yuePaiMapper.getYuepaiByShoucangYId(id);
        for (Yuepai yuepai : list) {
            List list1 = JSON.parseObject(yuepai.getUrls(), List.class);
            yuepai.setUrlsList(list1);
            List list2 = JSON.parseObject(yuepai.getPTags(), List.class);
            yuepai.setPTagsList(list2);
        }
        return list;
    }
}
