package com.gongyou.forum.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gongyou.common.pojo.Zuopin;
import com.gongyou.forum.mapper.ZuopinMapper;
import com.gongyou.forum.service.ZuopinService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author LiQuanPing
 * @Date 2021-04-12 9:59
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
@Service
public class ZuopinServiceImpl implements ZuopinService {


    @Autowired
    private ZuopinMapper zuopinMapper;

    //读取所有作品列表
    @Override
    public List getZuopinAll() {
        List<Zuopin> zuopins = zuopinMapper.selectList(new QueryWrapper<Zuopin>().eq("is_del", 0).orderByDesc("pub_date"));
        for (Zuopin zuopin : zuopins) {
            String urls = zuopin.getUrls();
            List list = JSON.parseObject(urls, List.class);
            zuopin.setUrlsList(list);
        }
        return zuopins;
    }

    //根据作品id读取作品详情
    @Override
    public Zuopin getZuopinDetail(String id) {
        Zuopin zuopin = zuopinMapper.selectById(id);
        List list = JSON.parseObject(zuopin.getZTags(), List.class);
        zuopin.setZTagsList(list);
        List list1 = JSON.parseObject(zuopin.getUrls(), List.class);
        zuopin.setUrlsList(list1);
        return zuopin;
    }

    //根据作品id增加作品浏览量
    @Override
    public void addZuopinView(String id) {
        Zuopin zuopin = zuopinMapper.selectById(id);
        zuopin.setView(zuopin.getView()+1);
        zuopinMapper.updateById(zuopin);
    }

    //根据作品用户id读取所有的作品列表
    @Override
    public Page getZuopinById(String uid, String currentPage) {
        long l = Long.parseLong(currentPage);
        Page page = new Page(0,l*10);
        Page<Zuopin> page1 = zuopinMapper.selectPage(page,new QueryWrapper<Zuopin>().eq("z_id", uid).eq("is_del", 0).orderByDesc("pub_date"));
        List<Zuopin> zuopins = page1.getRecords();
        for (Zuopin zuopin : zuopins) {
            List list = JSON.parseObject(zuopin.getUrls(), List.class);
            zuopin.setUrlsList(list);
            List list1 = JSON.parseObject(zuopin.getZTags(), List.class);
            zuopin.setZTagsList(list1);
        }
        return page1.setRecords(zuopins);
    }

    @Override
    public List getZuopinById(String uid) {
        List<Zuopin> zuopins = zuopinMapper.selectList(new QueryWrapper<Zuopin>().eq("z_id", uid).eq("is_del", 0).orderByDesc("pub_date"));
        for (Zuopin zuopin : zuopins) {
            List list = JSON.parseObject(zuopin.getUrls(), List.class);
            zuopin.setUrlsList(list);
            List list1 = JSON.parseObject(zuopin.getZTags(), List.class);
            zuopin.setZTagsList(list1);
        }
        return zuopins;
    }

    //根据id删除作品
    @Override
    public void delZuopinById(String id) {
        Zuopin zuopin = new Zuopin();
        zuopin.setId(Long.parseLong(id));
        zuopin.setIsDel(1);
        zuopinMapper.updateById(zuopin);
    }

    //发布作品相册，保存信息
    @Override
    public void pulishZuopin(Zuopin zuopin) {
        zuopinMapper.insert(zuopin);
    }
}
