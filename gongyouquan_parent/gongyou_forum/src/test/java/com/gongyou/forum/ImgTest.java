package com.gongyou.forum;

import com.gongyou.common.utils.AvaImgHanlderUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

/**
 * @Author LiQuanPing
 * @Date 2021-04-14 14:39
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ForumApplication.class)
public class ImgTest {


    @Value("${avaImg.imgPath}")
    private String path;

    @Test
    public void test1() throws IOException {
        //System.out.println(path);
        String name = "zhangsan.png";
        AvaImgHanlderUtils.generateImg("张三",path+name,name);

    }
}
