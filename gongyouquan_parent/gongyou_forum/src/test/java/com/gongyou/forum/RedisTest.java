package com.gongyou.forum;

import com.alibaba.fastjson.JSON;
import com.gongyou.common.vo.UserVo;
import com.gongyou.forum.service.UserInfoService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author LiQuanPing
 * @Date 2021-04-02 12:41
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ForumApplication.class)
public class RedisTest {




    @Autowired
    private StringRedisTemplate redisTemplate;


    @Test
    public void test(){

//        Boolean name = redisTemplate.delete("name");
//        System.out.println(name);
        redisTemplate.opsForValue().set("name","32");
     //   System.out.println(name);

    }
    //递增和递减
    @Test
    public void test1(){
        redisTemplate.boundValueOps("name").increment(-3L);
    }

    //添加一个map
    @Test
    public void test2(){
   /*     List list = new ArrayList();
        list.add("zhangsan");
        String s = JSON.toJSONString(list);
        redisTemplate.opsForHash().put("hash",s,"22");
*/

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("bbb","bb");
        redisTemplate.boundHashOps("HashKey").putAll(hashMap );

    }
    //获取所有map
    @Test
    public void test3(){

        Map<Object, Object> hashKey = redisTemplate.opsForHash().entries("HashKey");
        System.out.println(hashKey);

    }

    //操作list
    @Test
    public void test4(){

        redisTemplate.opsForList().leftPush("list","bbbbbbb");
    }

    //操作set
    @Test
    public void test5(){

        redisTemplate.boundSetOps("set").add("1");
        redisTemplate.boundSetOps("set").add("2");
        redisTemplate.boundSetOps("set").add("3");
    }

    //根据value从一个set中查询,是否存在
    @Test
    public void test6(){
        Boolean set = redisTemplate.boundSetOps("set").isMember("2");
        System.out.println(set);
    }

    //移除指定的元素
    @Test
    public void test7(){

        Long set = redisTemplate.boundSetOps("set").remove("2");
        System.out.println(set);
    }



}
