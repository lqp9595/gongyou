package com.gongyou.gateway;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;



/**
 * @Author LiQuanPing
 * @Date 2021-03-19 11:50
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 * 网关模块---
 */
@SpringBootApplication
@EnableEurekaClient
public class GatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class, args);
    }

    /*  bean在这里会有先后顺序   */

    @Bean
    @Primary
    public KeyResolver ipKeyResolver() {
        return new KeyResolver() {
            @Override
            public Mono<String> resolve(ServerWebExchange exchange) {
              //  System.out.println("本机地址是" + exchange.getRequest().getRemoteAddress().getHostName());
                return Mono.just(exchange.getRequest().getRemoteAddress().getHostName());
            }
        };
    }

    @Bean
    public KeyResolver pathKeyResolver() {
        //写法1
 /*       return exchange -> Mono.just(
                exchange.getRequest()
                        .getPath()
                        .toString()
        );*/

        return new KeyResolver() {
            @Override
            public Mono<String> resolve(ServerWebExchange exchange) {

                System.out.println("请求路劲是" + exchange.getRequest().getPath().toString());
                return Mono.just(exchange.getRequest()
                        .getPath()
                        .toString());
            }

        };

    }

  /*  // 如果没有使用默认值80
    @Value("${http.port:80}")
    Integer httpPort;

    // 正常启用的https端口 如443
    @Value("${server.port}")
    Integer httpsPort;

    // springboot2 写法
    @Bean
    @ConditionalOnProperty(name = "condition.http2https", havingValue = "true", matchIfMissing = false)
    public TomcatServletWebServerFactory servletContainer() {
        TomcatServletWebServerFactory tomcat = new TomcatServletWebServerFactory() {
            @Override
            protected void postProcessContext(Context context) {
                SecurityConstraint constraint = new SecurityConstraint();
                constraint.setUserConstraint("CONFIDENTIAL");
                SecurityCollection collection = new SecurityCollection();
                collection.addPattern("/*");
                constraint.addCollection(collection);
                context.addConstraint(constraint);
            }
        };
        tomcat.addAdditionalTomcatConnectors(httpConnector());
        return tomcat;
    }

    @Bean
    @ConditionalOnProperty(name = "condition.http2https", havingValue = "true", matchIfMissing = false)
    public Connector httpConnector() {
        System.out.println("启用http转https协议，http端口：" + this.httpPort + "，https端口：" + this.httpsPort);
        Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
        connector.setScheme("http");
        // Connector监听的http的端口号
        connector.setPort(httpPort);
        connector.setSecure(false);
        // 监听到http的端口号后转向到的https的端口号
        connector.setRedirectPort(httpsPort);
        return connector;
    }*/

}
