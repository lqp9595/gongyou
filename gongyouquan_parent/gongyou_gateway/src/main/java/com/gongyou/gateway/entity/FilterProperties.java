package com.gongyou.gateway.entity;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author LiQuanPing
 * @Date 2021-04-02 9:43
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 */
@Data
@Component
@ConfigurationProperties(prefix = "filterspath")
public class FilterProperties {


    private List allowPaths;

}
