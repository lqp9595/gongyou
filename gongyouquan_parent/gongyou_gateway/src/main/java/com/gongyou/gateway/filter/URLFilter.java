package com.gongyou.gateway.filter;//package com.yzgc.filter;
//
//import org.springframework.cloud.gateway.filter.GatewayFilterChain;
//import org.springframework.cloud.gateway.filter.GlobalFilter;
//import org.springframework.core.Ordered;
//import org.springframework.http.server.reactive.ServerHttpRequest;
//import org.springframework.stereotype.Component;
//import org.springframework.web.server.ServerWebExchange;
//import reactor.core.publisher.Mono;
//
///**
// * @Author LiQuanPing
// * @Date 2020-07-30 22:28
// * @Version 1.8
// * @creed: ♂♂Talk is cheap,show me the code♀♀
// */
//@Component
//public class URLFilter implements GlobalFilter, Ordered {
//    @Override
//    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
//        System.out.println("经过地址过滤器");
//        ServerHttpRequest request = exchange.getRequest();
//       // String path = request.getURI().getPath();
//        String path = request.getURI().getPath();
//       // if (path.contains(".css") || path.contains(".js") || path.contains("/image") || path.contains("/lib") || path.contains("/fonts")){
//       // }
//      String ipName = request.getRemoteAddress().getHostName();
//        System.out.println("ip地址是"+ipName);
//            System.out.println("访问地址是"+path);
//        //放行
//        return chain.filter(exchange);
//    }
//
//    @Override
//    public int getOrder() {
//        return 2;
//    }
//}
