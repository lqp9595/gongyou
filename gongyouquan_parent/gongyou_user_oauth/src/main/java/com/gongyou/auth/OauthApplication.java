package com.gongyou.auth;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @Author LiQuanPing
 * @Date 2021-03-19 11:56
 * @Version 1.8
 * @creed: ♂♂Talk is cheap,show me the code♀♀
 * 此模块木投入使用,可以不启动
 */
@SpringBootApplication
@EnableEurekaClient
public class OauthApplication {
    public static void main(String[] args) {
        SpringApplication.run(OauthApplication.class,args);
    }

}
